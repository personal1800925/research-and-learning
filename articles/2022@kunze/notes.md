# Towards Explainable and Trustworthy Collaborative Robots through Embodied Question Answering 

## TL;DR

This article emphasizes the importance of enabling robots, that work alongside humans, to explain their actions, thereby facilitating the identification of the causes behind any incidents or accidents. The authors express a strong conviction that having explainable robots is ethically obligatory and leads to the creation of reliable and trustworthy robots. To accomplish this, they propose a framework for developing a rapid system that can analyze the causes of incidents, taking into account the perspectives of different stakeholders and their varying levels of knowledge. This system is built upon a combination of language models and knowledge graphs.

![graphical asbtract](./ga.png)

## Key Insights

- As human-robot collaboration continues to gain momentum, incidents or accidents are inevitably bound to occur.
- Swift understanding of the causes behind these incidents or accidents is crucial in order to rectify the issue and prevent further damage.
- Consequently, robots should possess the capability to retrieve historical data and knowledge concerning the causes of specific actions, akin to an airplane's black box. The authors refer to this as the Ethical Black Box (EBB); it's deemed 'ethical' because it should be a mandatory component in robots, not because the robot makes ethical decisions.
- Analysis using EBB allows for safer robot operations, as it enables the identification of problem causes, assigns accountability for incidents/accidents (i.e., developers, workers, etc.), and enhances the robots' trustworthiness by providing insights into their decision-making process.
- Given the need for rapid problem resolution, the authors introduce a framework based on knowledge graphs and language models to question the robot about its decisions using the EBB; hence, the embodied question-answering solution.
- The solution is described as 'embodied' because it should be an integral part of the robot.

## Research Questions / Hypothesis

Not really a research article

## Future Research Directions

- Real experiments and not just the presentation of the framework. It's kind of a position article
 
## Critical Evaluation

### Strengths

- The idea and the arguments behind EBB 

### Weaknesses

- No experiments or evaluations 

