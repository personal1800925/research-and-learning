# Research and Learning Repository

This repository serves to compile and organize notes derived from a variety
of research articles and projects. It also includes notes and code snippets
that aid in the understanding of new concepts or topics.

---

## Research Article Reviews

| Year | Journal/Conference | Stars  | Name                                                                                         | Links                                                                                                                     |
|:----:|--------------------|--------|----------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| 2022 | ICRA WORKSHOP      | *      | Towards Explainable and Trustworthy Collaborative Robots through Embodied Question Answering | [Article](https://ora.ox.ac.uk/objects/uuid:4d8079cb-7633-417d-ba9b-d580aaebff64) [Notes](./articles/2022@kunze/notes.md) |

